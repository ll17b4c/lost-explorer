#ifndef __GAME_WIDGET_H__
#define __GAME_WIDGET_H__ 1

#include <QObject>
#include <QtWidgets>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QPushButton>


class GameOptionsWidget: public QWidget
	{ //

	  	Q_OBJECT

	public:
	GameOptionsWidget(QWidget *parent);
	~GameOptionsWidget();


	QPushButton *back, *start;


	protected:


	private:
	QGroupBox *createRulesOptionsGroup();
	QGroupBox *createAIOptionsGroup();
	QGroupBox *rulesOptionsGroup, *aiOptionsGroup;
	QVBoxLayout *menuLayout;
	};

#endif
