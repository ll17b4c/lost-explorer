#include <QApplication>
#include <QVBoxLayout>
#include "LostExplorerWindow.h"

int main(int argc, char *argv[])
	{ // main
	// create the application
	QApplication app(argc, argv);

  	LostExplorerWindow *window = new LostExplorerWindow(NULL);//make game window

	window->resize(800, 800);// resize the window

	window->show();// show the label

	app.exec();

	delete window;// clean up

	return 0;
	} // main
