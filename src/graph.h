#ifndef GRAPH_H
#define GRAPH_H
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/properties.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/named_function_params.hpp>
#include <boost/graph/iteration_macros.hpp>


struct VertexData
{
  std::string vertex_name;
  int num;
};

struct EdgeData
{
  std::string edge_name;
  double dist;
};

typedef boost::property<boost::edge_weight_t, int> EdgeWeightProperty;


typedef boost::adjacency_list
	<boost::vecS,
	boost::vecS,
    boost::undirectedS,
	boost::no_property,
	EdgeWeightProperty>
	caveGraphType;


typedef boost::graph_traits<caveGraphType>::edge_iterator edge_iterator;
typedef boost::graph_traits<caveGraphType>::vertex_iterator vertex_iterator;




#endif // GRAPH_H
