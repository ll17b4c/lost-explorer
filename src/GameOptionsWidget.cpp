#include <QDebug>
#include <QVBoxLayout>
#include <QRadioButton>
#include <QGroupBox>
#include "GameOptionsWidget.h"





// constructor
GameOptionsWidget::GameOptionsWidget(QWidget *parent)
    : QWidget(parent)

	{ // constructor
    menuLayout = new QVBoxLayout(this);
    this->setLayout(menuLayout);

    start = new QPushButton("Start");
    menuLayout->addWidget(start);

    menuLayout->addWidget(createRulesOptionsGroup());
    menuLayout->addWidget(createAIOptionsGroup());

    back = new QPushButton("Back");
    menuLayout->addWidget(back);
    } // constructor

QGroupBox *GameOptionsWidget::createRulesOptionsGroup()
{
    rulesOptionsGroup = new QGroupBox(tr("Rules Options"));

    QRadioButton *radio1 = new QRadioButton(tr("Rad&io button 1"));
    QRadioButton *radio2 = new QRadioButton(tr("Radi&o button 2"));
    QRadioButton *radio3 = new QRadioButton(tr("Radio &button 3"));
    radio1->setChecked(true);

    QVBoxLayout *gameOptionsLayout = new QVBoxLayout();

    gameOptionsLayout->addWidget(radio1);
    gameOptionsLayout->addWidget(radio2);
    gameOptionsLayout->addWidget(radio3);
    gameOptionsLayout->addStretch(1);

    rulesOptionsGroup->setLayout(gameOptionsLayout);

    return rulesOptionsGroup;
}

QGroupBox *GameOptionsWidget::createAIOptionsGroup()
{
    aiOptionsGroup = new QGroupBox(tr("A.I. Options"));

    QRadioButton *radio1 = new QRadioButton(tr("Rad&io button 1"));
    QRadioButton *radio2 = new QRadioButton(tr("Radi&o button 2"));
    QRadioButton *radio3 = new QRadioButton(tr("Radio &button 3"));
    radio1->setChecked(true);

    QVBoxLayout *aiOptionsLayout = new QVBoxLayout();

    aiOptionsLayout->addWidget(radio1);
    aiOptionsLayout->addWidget(radio2);
    aiOptionsLayout->addWidget(radio3);
    aiOptionsLayout->addStretch(1);

    aiOptionsGroup->setLayout(aiOptionsLayout);

    return aiOptionsGroup;
}

GameOptionsWidget::~GameOptionsWidget()
    { // destructor
        delete back;
        delete start;
        delete menuLayout;
        //delete rulesOptionsGroup;
        //delete aiOptionsGroup;
  	} // destructor
