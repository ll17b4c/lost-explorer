#ifndef __LOST_WINDOW_H__
#define __LOST_WINDOW_H__ 1

#include <QtWidgets>
#include <QMenuBar>
#include <QBoxLayout>
#include "LostGameWidget.h"
#include "MainMenuWidget.h"
#include "GameOptionsWidget.h"
#include "SettingsWidget.h"

class LostExplorerWindow: public QWidget
	{

		Q_OBJECT

	public:
	// constructor / destructor
	LostExplorerWindow(QWidget *parent);
	~LostExplorerWindow();

	// menu bar
	QMenuBar *menuBar;
		// file menu
		QMenu *fileMenu;
			// quit action
			QAction *actionQuit;


	QBoxLayout *windowLayout; // window layout

	MainMenuWidget *mainWidget; // widget for main menu
	GameOptionsWidget *optionsWidget; // widget for options
	SettingsWidget *settingsWidget; // widget for settings
	LostGameWidget *gameWidget; // widget for game

	// resets all the interface elements
	void ResetInterface();

	protected:

	void closeMain();
	void closeGameOptions();
	void closeSettings();
	void closeGame();

	void openMain();
	void openGameOptions();
	void openSettings();
	void openGame();

	public slots:


	void mainToGameOptions();
	void gameOptionsToMain();
	void gameOptionsToGame();
	void gameToGameOptions();
	// void mainToGameOptions();
	// void gameToGameOptions();

	};

#endif
