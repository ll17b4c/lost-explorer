#include <QDebug>
#include "MainMenuWidget.h"


// constructor
MainMenuWidget::MainMenuWidget(QWidget *parent)
    : QWidget(parent)

	{ // constructor
    menuLayout = new QVBoxLayout(this);
    this->setLayout(menuLayout);

    selectAI = new QPushButton("Play Verses A.I.");
    menuLayout->addWidget(selectAI);

    selectHuman = new QPushButton("Play Versus Another Player");
    menuLayout->addWidget(selectHuman);

    selectSettings = new QPushButton("Settings");
    menuLayout->addWidget(selectSettings);

    selectExit = new QPushButton("Exit");
    menuLayout->addWidget(selectExit);

    } // constructor


MainMenuWidget::~MainMenuWidget()
    { // destructor
    delete selectAI;
  	delete selectHuman;
  	delete selectSettings;
  	delete selectExit;
  	} // destructor
