#ifndef __MAIN_WIDGET_H__
#define __MAIN_WIDGET_H__ 1

#include <QObject>
#include <QtWidgets>
#include <QVBoxLayout>
#include <QPushButton>


class MainMenuWidget: public QWidget
	{ //

	  	Q_OBJECT

	public:
	MainMenuWidget(QWidget *parent);
	~MainMenuWidget();


	QVBoxLayout * menuLayout;

	QPushButton *selectAI, *selectHuman, *selectSettings, *selectExit;


	protected:


	private:


	};

#endif
