#ifndef __LOST_WIDGET_H__
#define __LOST_WIDGET_H__ 1

#include <QObject>
#include <QtWidgets>
#include <string>
#include <fstream>
#include "gamegui.h"
#include "graph.h"


class LostGameWidget: public QWidget
	{ //

	  Q_OBJECT

	public:
	LostGameWidget(QWidget *parent);
	QPushButton *back;

	protected:
	GameGUI *graph;
	void makeGraph(caveGraphType * cave);


	private:
	QVBoxLayout *menuLayout;
	caveGraphType * cave;



	};

#endif
