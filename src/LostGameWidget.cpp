#include <QDebug>

#include "LostGameWidget.h"
#include "gamegui.h"


// constructor
LostGameWidget::LostGameWidget(QWidget *parent)
  : QWidget(parent)

	{ // constructor

    menuLayout = new QVBoxLayout(this);
    this->setLayout(menuLayout);
    back = new QPushButton("Back");
    menuLayout->addWidget(back);



    caveGraphType c(6);
    cave = &c;
    makeGraph(cave);


    GameGUI *graph = new GameGUI(this,cave);
    menuLayout->addWidget(graph);

	} // constructor

void LostGameWidget::makeGraph(caveGraphType * cave)
{
    //caveGraphType cave(6);

    //Graph g(edge_array, edge_array + sizeof(edge_array) / sizeof(Edge), num_vertices);
    //enum { A, B, C, D, E, N };
 // Create graph with 100 nodes and edges with probability 0.05
    boost::add_edge(0, 1, *cave);
    boost::add_edge(0, 2, *cave);
    boost::add_edge(0, 4, *cave);
    boost::add_edge(1, 3, *cave);
    boost::add_edge(2, 4, *cave);
    boost::add_edge(3, 2, *cave);
    boost::add_edge(4, 2, *cave);
    boost::add_edge(0, 3, *cave);
    boost::add_edge(5, 0, *cave);
    boost::add_edge(3, 5, *cave);
    boost::add_edge(5, 2, *cave);



}
