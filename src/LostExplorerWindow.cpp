#include "LostExplorerWindow.h"




#include <iostream>
using namespace std;


// constructor / destructor
LostExplorerWindow::LostExplorerWindow(QWidget *parent)
	: QWidget(parent)
	{ // constructor

	// create menu bar
	menuBar = new QMenuBar(this);
	fileMenu = menuBar->addMenu("&File");
	// quit action
	actionQuit = new QAction("&Quit", this);
	fileMenu->addAction(actionQuit);

	// create the window layout
	windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);

	openMain();

	} // constructor

LostExplorerWindow::~LostExplorerWindow()
	{ // destructor
	//delete mainWidget;
	//delete optionsWidget;
	//delete settingsWidget;
	//delete gameWidget;
	delete windowLayout;
	delete actionQuit;
	delete fileMenu;
	delete menuBar;
	} // destructor

void LostExplorerWindow::closeMain(){delete mainWidget;}
void LostExplorerWindow::closeGameOptions(){delete optionsWidget;}
void LostExplorerWindow::closeSettings(){delete settingsWidget;}
void LostExplorerWindow::closeGame(){delete gameWidget;}





//Setup methods for opening menu widgets
void LostExplorerWindow::openMain()
{
	mainWidget = new MainMenuWidget(this);
	windowLayout->addWidget(mainWidget);
	connect(mainWidget->selectAI, SIGNAL(clicked()), this, SLOT(mainToGameOptions()));
}

void LostExplorerWindow::openGameOptions()
{
	optionsWidget = new GameOptionsWidget(this); // widget for game
	windowLayout->addWidget(optionsWidget);
	connect(optionsWidget->back, SIGNAL(clicked()), this, SLOT(gameOptionsToMain()));
	connect(optionsWidget->start, SIGNAL(clicked()), this, SLOT(gameOptionsToGame()));
}

void LostExplorerWindow::openSettings()
{
	settingsWidget = new SettingsWidget(this); // widget for game
	windowLayout->addWidget(settingsWidget);
}

void LostExplorerWindow::openGame()
{
	gameWidget = new LostGameWidget(this); // widget for game

	windowLayout->addWidget(gameWidget);
	connect(gameWidget->back, SIGNAL(clicked()), this, SLOT(gameToGameOptions()));
}



//Methods for transfering from one menu to another
void LostExplorerWindow::mainToGameOptions()
{
	closeMain();
	openGameOptions();
}

void LostExplorerWindow::gameOptionsToMain()
{
	closeGameOptions();
	openMain();
}

void LostExplorerWindow::gameOptionsToGame()
{

	closeGameOptions();
	openGame();
}

void LostExplorerWindow::gameToGameOptions()
{
	closeGame();
	openGameOptions();
}




// resets all the interface elements
void LostExplorerWindow::ResetInterface()
	{ // ResetInterface
	gameWidget->update();
	update();
	} // ResetInterface
