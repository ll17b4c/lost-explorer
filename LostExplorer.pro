TEMPLATE = app


TARGET = LostExplorer
INCLUDEPATH += . /opt/local/include

CONFIG += C++11



QT += widgets gui

LIBS += -lGLU
# Input
HEADERS +=  src/LostExplorerWindow.h \
            src/MainMenuWidget.h \
            src/GameOptionsWidget.h \
            src/SettingsWidget.h \
            src/graph.h \
            src/LostGameWidget.h \
            src/gameoperations.h \
            src/gamegui.h \
            src/node.h \
            src/edge.h

SOURCES +=  src/LostExplorerMain.cpp \
            src/LostExplorerWindow.cpp \
            src/MainMenuWidget.cpp \
            src/GameOptionsWidget.cpp \
            src/SettingsWidget.cpp \
            src/LostGameWidget.cpp \
            src/gameoperations.cpp \
            src/gamegui.cpp \
            src/node.cpp \
            src/edge.cpp

DESTDIR = bin
OBJECTS_DIR = bin/.obj
MOC_DIR = bin/.moc
RCC_DIR = bin/.rcc
UI_DIR = bin/.ui
